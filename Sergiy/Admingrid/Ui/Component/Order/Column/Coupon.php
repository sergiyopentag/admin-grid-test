<?php

namespace Sergiy\Admingrid\Ui\Component\Order\Column;

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;

class Coupon extends Column
{
    protected $_orderRepository;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, OrderRepositoryInterface $orderRepository, array $components = [], array $data = [])
    {
        $this->_orderRepository = $orderRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $dataSourceItem) {

                $order  = $this->_orderRepository->get($dataSourceItem["entity_id"]);
                $coupon = $order->getData("coupon_code");
                $dataSourceItem[$this->getData('name')] = $coupon;
            }
        }

        return $dataSource;
    }
}